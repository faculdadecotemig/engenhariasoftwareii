from django.db import models
from team.models import Team
from location.models import Location
from arbiter.models import Arbiter


# Create your models here.
class Match(models.Model):
    home_team = models.ForeignKey(Team, related_name="home_team", on_delete=models.SET_NULL, null=True)
    home_points = models.IntegerField()
    home_goals = models.IntegerField()
    visitor_team = models.ForeignKey(Team, related_name="visitor_team", on_delete=models.SET_NULL, null=True)
    visitor_points = models.IntegerField()    
    visitor_goals = models.IntegerField()    
    location = models.ForeignKey(Location, on_delete=models.SET_NULL, null=True)
    happens_at = models.DateTimeField(null=False)
    arbiters = models.ManyToManyField(Arbiter)
    # rodada
    # campeonato