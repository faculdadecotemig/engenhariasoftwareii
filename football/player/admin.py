from django.contrib import admin

from player.models import Player
from location.models import Location
from team.models import Team
from arbiter.models import Arbiter
from match.models import Match


admin.site.register(Player)
admin.site.register(Location)
admin.site.register(Team)
admin.site.register(Arbiter)
admin.site.register(Match)