from django.shortcuts import render
from match.models import Match
from arbiter.models import Arbiter
from team.models import Team
from player.models import Player
from arbiter.models import Arbiter


def matchs(request):
    #lista = [{
    #    'name': 'Eugenio',
    #    'batata': 1,
    #},{
    #    'name': 'Igor',
    #    'batata': 2,
    #}]

    
    
    #for x in lista:
    #    Player.objects.create(name=x['name'], team_id=x['batata'])

    ctx = {
        'matchs': Match.objects.all() 
    }
    return render(request, 'football/matchs.html', ctx)

def home(request):

    return render(request, 'football/home.html')

def players(request):

    return render(request, 'football/players.html')

def arbiters(request):
    ctx = {
        'arbiters': Arbiter.objects.all() 
    }
    return render(request, 'football/arbiters.html', ctx)

def saveArbiter(arbiterTobeSaved):
        Arbiter.objects.create(name= arbiterTobeSaved)   

def teams(request):
   
    return render(request, 'football/teams.html')
